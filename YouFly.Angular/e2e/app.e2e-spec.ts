import { PhaseFinalAdminPage } from './app.po';

describe('phase-final-admin App', () => {
  let page: PhaseFinalAdminPage;

  beforeEach(() => {
    page = new PhaseFinalAdminPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
