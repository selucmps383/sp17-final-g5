import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ComponentSearchResultsComponent } from "app/component-search-results/component-search-results.component";
import { ComponentFlightInquiryComponent } from "app/component-flight-inquiry/component-flight-inquiry.component";
import { LogInFormComponent } from "app/log-in-form/log-in-form.component";

const routes: Routes = [
    { path: 'clientapp', component: ComponentSearchResultsComponent },
    { path: 'search', component: ComponentFlightInquiryComponent },
    { path: "log-in-form", component: LogInFormComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }