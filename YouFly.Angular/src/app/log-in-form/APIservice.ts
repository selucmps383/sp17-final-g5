import { Injectable, Inject } from '@angular/core';
import {flight} from '../flight';
import { Request, RequestOptions, Http, Headers, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
//import { FlightDBINFO } from ;
@Injectable()
export class ApiService {

  private headers = new Headers({
    "Content-Type": "application/json",
    "Accept": "application/json"
  })

  constructor(private http: Http) { }

  apiUrl = 'http://localhost:60770/api/flights';
  //Name : FlightDBINFO; 
  //Name: FlightDBINFO;

 // tester():  Observable<>{
  //return this.http.get("http://localhost:65081/api/flights/test");

  public GetFlights(): Observable<flight> {
    return this.http.get(this.apiUrl)
      .map(res => res.json())
  }
}



  
 //// }
//}



