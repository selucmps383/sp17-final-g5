import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from './APIservice';
//import { Flight } from '../flight';


@Component({
  selector: 'app-log-in-form',
  templateUrl:'./log-in-form.component.html',
  styleUrls: ['./log-in-form.component.css']
})

export class LogInFormComponent {


  private loginForm: FormGroup;
  private Airline = '';
  private Depart = '';
  private Destination = '';
  private testtt = '';
  public flights;
  private dbemail = '';
  private dbpassword = '';
  private customerdata;
  //setting up to get the variables from the form

  constructor(private formBuilder: FormBuilder, private apiService: ApiService) {
    this.apiService.GetFlights()
    .subscribe(response => {
      this.flights = response; 
      console.log(response);
    });
    console.log();
    
    
    this.loginForm = this.formBuilder.group({
      Airline: this.flights,
      Depart: '',
      Email: '',
      Password: '',
      Destination: ''
    });
  }

  onSubmit() {

    console.log(this.flights[0]);
    this.loginForm = this.formBuilder.group({
      Airline: this.flights[0].name,
      Depart: this.flights[0].departIATA,
      Destination: ''
    });
    
    this.flights
    
     //below uses testtt to test if the variables work as they should, as variables to manipulate
    console.log(this.testtt);


    // this.doestheEmailandPasswordMatch;
  }

  //the console.log(this.testtt) shows the two variables emailuse and passworduse combined as should be now, disregard testtt unless testing the variables



  doestheEmailandPasswordMatch() {
    this.Airline = this.loginForm.controls['Airline'].value;
    this.Depart = this.loginForm.controls['Depart'].value;
    this.Destination = this.loginForm.controls['Destination'].value;
    console.log(this.loginForm.controls['Destination'].value);
    this.testtt = this.Airline + " " + this.Depart + " " + this.Destination;

    /*  if(this.emailuse!=null){
         console.log("THE USER EXISTS");
         //the following before the else statement should be another if() to check if the email and password match, like if((this.emailuse==whateverindatabase) && (this.passworduse==whateverisonsamedatabaserowastheemail)
        
         if(this.testtt == this.customerdata){
             console.log("user matched");
             //load landing page and do the cookie
             }else{
               console.log("THERE ARE NO MATCHING USERS WITH THIS NAME");
               //load error page  
              }
     
 
           }*/

  }
  thisisaTESTmethod() {
    this.Airline = this.loginForm.controls['Airline'].value;
    this.Depart = this.loginForm.controls['Depart'].value;
    this.Destination = this.loginForm.controls['Destination'].value;

    /*  if(this.emailuse!=null){
          ApiService
      }else{
        console.log("THERE ARE NO MATCHING USERS WITH THIS NAME");
              //load error page
      }
    }*/
  }
}
