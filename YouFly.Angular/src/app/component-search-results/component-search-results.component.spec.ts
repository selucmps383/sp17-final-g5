import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentSearchResultsComponent } from './component-search-results.component';

describe('ComponentSearchResultsComponent', () => {
  let component: ComponentSearchResultsComponent;
  let fixture: ComponentFixture<ComponentSearchResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentSearchResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentSearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
