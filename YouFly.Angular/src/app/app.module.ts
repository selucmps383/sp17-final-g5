import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ApiService } from './log-in-form/APIservice'
import { AppComponent } from './app.component';
import { LogInFormComponent } from './log-in-form/log-in-form.component';
import { RouterModule } from '@angular/router';


import { ComponentFlightInquiryComponent } from './component-flight-inquiry/component-flight-inquiry.component';
import { ComponentSearchResultsComponent } from './component-search-results/component-search-results.component'
import { AppRoutingModule } from "app/router/router.module";

@NgModule({
    declarations: [
        AppComponent,
        LogInFormComponent,
        ComponentFlightInquiryComponent,
        ComponentSearchResultsComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        ReactiveFormsModule,
        HttpModule
    ],


    providers: [ApiService],
    bootstrap: [AppComponent]
})
export class AppModule { }
