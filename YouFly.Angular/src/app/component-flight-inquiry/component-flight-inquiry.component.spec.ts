import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentFlightInquiryComponent } from './component-flight-inquiry.component';

describe('ComponentFlightInquiryComponent', () => {
  let component: ComponentFlightInquiryComponent;
  let fixture: ComponentFixture<ComponentFlightInquiryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentFlightInquiryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentFlightInquiryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
