import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: 'app-component-flight-inquiry',
  templateUrl:'./component-flight-inquiry.component.html',
  styleUrls: ['./component-flight-inquiry.component.css']
  
})
export class ComponentFlightInquiryComponent implements OnInit {

  constructor(private router: Router) { 
    
  }

  ngOnInit() {
  }
  
  flightInquiry = new FormGroup({
       From: new FormControl(),
       To: new FormControl(),
       NumberofPass: new FormControl(),
       Depart: new FormControl(),
       Return: new FormControl()
    });

   onSubmit() {
      this.router.navigate(["/log-in-form"])
      
   }


}
