interface FlightDBINFO{
Key: number;
Name: string;
departIATA : string;
arriveIATA : string;
Time : number;
Day: string;
Length : number;
hrsTravelled : DoubleRange;
milesTravelled: DoubleRange;
seatPrice : DoubleRange;
seatAmnt : number;
seatType : string;


}