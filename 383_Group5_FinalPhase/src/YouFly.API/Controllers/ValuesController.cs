﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YouFly.API.Models;
using YouFly.API.Repository;

namespace YouFly.API.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {

        private readonly IYouFlyRepository _youFlyRepo;

        public ValuesController(IYouFlyRepository youFlyRepo)
        {
            _youFlyRepo = youFlyRepo;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<Flights> GetAll()
        {
            return _youFlyRepo.GetAll();
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetFlight")]
        public IActionResult GetById(long id)
        {
            var item = _youFlyRepo.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        // POST api/values
        [HttpPost]
        public IActionResult Create([FromBody]Flights item)
        {
            if (item == null)
            {
                return BadRequest();
            }

            _youFlyRepo.Add(item);
            return CreatedAtRoute("GetFlight", new { id = item.Key }, item);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody]Flights item)
        {
            if (item == null || item.Key != id)
            {
                return BadRequest();
            }

            var flight = _youFlyRepo.Find(id);
            if (flight == null)
            {
                return NotFound();
            }

            flight.departIATA = item.departIATA;
            flight.arriveIATA = item.arriveIATA;
            flight.Day = item.Day;
            flight.seatType = item.seatType;
            flight.seatAmnt = item.seatAmnt;
            flight.seatPrice = item.seatPrice;
            flight.Time = item.Time;
            flight.milesTravelled = item.milesTravelled;
            flight.Length = item.Length;
            flight.hrsTravelled = item.hrsTravelled;
            flight.Key = item.Key;
            flight.Name = item.Name;

            _youFlyRepo.Update(flight);
            return new NoContentResult();
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var flight = _youFlyRepo.Find(id);
            if (flight == null)
            {
                return NotFound();
            }
            _youFlyRepo.Remove(id);
            return new NoContentResult();
        }
    }
}