﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using YouFly.API.Contexts;

namespace YouFly.API.Migrations
{
    [DbContext(typeof(FlightContext))]
    partial class FlightContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("YouFly.API.Models.Cust", b =>
                {
                    b.Property<int>("UserId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<int>("FlightId");

                    b.Property<long?>("FlightsKey");

                    b.Property<string>("Fname");

                    b.Property<string>("Lname");

                    b.Property<string>("Password")
                        .IsRequired();

                    b.Property<bool>("isAdmin");

                    b.HasKey("UserId");

                    b.HasIndex("FlightsKey");

                    b.ToTable("Customer");
                });

            modelBuilder.Entity("YouFly.API.Models.Flights", b =>
                {
                    b.Property<long>("Key")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("Day");

                    b.Property<int>("Length");

                    b.Property<string>("Name");

                    b.Property<int>("Time");

                    b.Property<string>("arriveIATA");

                    b.Property<string>("departIATA");

                    b.Property<double>("hrsTravelled");

                    b.Property<double>("milesTravelled");

                    b.Property<int>("seatAmnt");

                    b.Property<double>("seatPrice");

                    b.Property<string>("seatType");

                    b.HasKey("Key");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("Flight");
                });

            modelBuilder.Entity("YouFly.API.Models.Cust", b =>
                {
                    b.HasOne("YouFly.API.Models.Flights", "Flights")
                        .WithMany()
                        .HasForeignKey("FlightsKey");
                });
        }
    }
}
