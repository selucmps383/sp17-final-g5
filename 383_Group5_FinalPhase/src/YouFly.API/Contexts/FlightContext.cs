﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using YouFly.API.Models;

namespace YouFly.API.Contexts
{
    public class FlightContext : DbContext
    {
        public FlightContext(DbContextOptions<FlightContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Flights>()
                .HasIndex(b => b.Name).IsUnique();
        }

        public DbSet<Flights> Flight { get; set; }
        public DbSet<Cust> Customer { get; set; }
    }

}