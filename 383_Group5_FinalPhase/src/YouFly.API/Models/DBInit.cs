﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouFly.API.Contexts;

namespace YouFly.API.Models
{
    public class DBInit
    {
        public static void Init(FlightContext context)
        {
            context.Database.EnsureCreated();

            if (context.Flight.Any())
            {
                return;
            }

            /*var flightinitializee = new Flights[]
             {
                 new Flights {Name = "Louis Armstrong", departIATA = "MSY", arriveIATA = "DEN", seatPrice = 5.0, seatType = "First Class", hrsTravelled=5.5, Key=1, Length=3333, milesTravelled=666, seatAmnt=50, Time=10}
             };
             foreach (Flights f in flightinitializee)
             {
                 context.Flight.Add(f);
             }*/


            var cust = new Cust[]
            {
                new Cust {Email = "admin@selu.edu", Fname = "Admin", Lname = "Istrator", Password = "password", isAdmin = true}
            };
            foreach (Cust c in cust)
            {
                context.Customer.Add(c);
            }
            context.SaveChanges();
        }
    }
}