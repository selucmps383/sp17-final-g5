﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace YouFly.API.Models
{
    public class Flights
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Key { get; set; }
        public string Name { get; set; }
        public string departIATA { get; set; }
        public string arriveIATA { get; set; }
        public DateTime Time { get; set; }
        public DayOfWeek Day { get; set; }
        public int Length { get; set; }
        public double hrsTravelled { get; set; }
        public double milesTravelled { get; set; }
        public double seatPrice { get; set; }
        public int seatAmnt { get; set; }
        public string seatType { get; set; }


    }
    public class Cust
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity), Key]
        public int UserId { get; set; }
        public string Fname { get; set; }
        public string Lname { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        public bool isAdmin { get; set; } = false;

        public int FlightId { get; set; }
        public virtual Flights Flights { get; set; }
    }
}
