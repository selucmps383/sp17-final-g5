﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouFly.API.Models;

namespace YouFly.API.Repository
{
    public interface IYouFlyRepository
    {
        void Add(Flights item);
        IEnumerable<Flights> GetAll();
        Flights Find(long key);
        void Remove(long key);
        void Update(Flights item);

    }
}