﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouFly.API.Contexts;
using YouFly.API.Models;

namespace YouFly.API.Repository
{
    public class YouFlyRepository : IYouFlyRepository
    {
        private readonly FlightContext _context;
        public YouFlyRepository(FlightContext context)
        {
            _context = context;
            if (_context.Flight.Count() == 0)
                Add(new Flights
                {
                    Name = "GetFlight"
                    /*arriveIATA = "MIA", departIATA = "MSY",
                    hrsTravelled =10.5, Length = 10, milesTravelled = 3000, seatAmnt = 10, seatPrice = 500,
                    seatType = "First Class", Time = 10*/
                });
        }

        public IEnumerable<Flights> GetAll()
        {
            return _context.Flight.ToList();
        }

        public void Add(Flights item)
        {

            _context.Flight.Add(item);
            _context.SaveChanges();
        }

        public Flights Find(long key)
        {
            return _context.Flight.FirstOrDefault(t => t.Key == key);
        }


        public void Remove(long key)
        {
            var entity = _context.Flight.First(t => t.Key == key);
            _context.Flight.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(Flights item)
        {
            _context.Flight.Update(item);
            _context.SaveChanges();
        }
    }
}