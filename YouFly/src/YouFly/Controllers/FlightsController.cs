﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YouFly.Models;
using YouFly.Repository;

namespace YouFly.Controllers
{
    [Route("api/[controller]")]
    public class FlightsController : Controller
    {
        private readonly IYouFlyRepository _youFlyRepository;

        public FlightsController(IYouFlyRepository YouFlyRepository)
        {
            _youFlyRepository = YouFlyRepository;
        }

        [HttpGet]
        public IEnumerable<Flights> GetAll()
        {
            return _youFlyRepository.GetAll();
        }

        [HttpGet("{id}", Name = "GetFlights")]
        public IActionResult GetById(long id)
        {
            var item = _youFlyRepository.Find(id);
            if(item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Flights item)
        {
            if(item == null)
            {
                return BadRequest();
            }

            _youFlyRepository.Add(item);
            return CreatedAtRoute("GetFlights", new { id = item.Key }, item);
        }

        [HttpPut]
        public IActionResult Update(long id, [FromBody] Flights item)
        {
            if (item == null || item.Key != id)
            {
                return BadRequest();
            }

            var youfly = _youFlyRepository.Find(id);
            if(youfly == null)
            {
                return NotFound();
            }

            youfly.departIATA = item.departIATA;
            youfly.Name = item.Name;

            _youFlyRepository.Update(youfly);
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var youfly = _youFlyRepository.Find(id);
            if (youfly == null)
            {
                return NotFound();
            }

            _youFlyRepository.Remove(id);
            return new NoContentResult();
        }
    }  
    
}
