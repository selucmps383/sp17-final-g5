﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouFly.Models;

namespace YouFly.Repository
{
  public class YouFlyRepository : IYouFlyRepository
  {
    private readonly FlightContext _context;
    // static List<Flights> FlightsList = new List<Flights>();
    public YouFlyRepository(FlightContext context)
    {
      _context = context;
      if (!_context.Set<Flights>().Any())
      {
        Add(new Flights { Name = "Item1" });
      }
    }

    public IEnumerable<Flights> GetAll()
    {
      return _context.Flight.ToList();
    }

    public void Add(Flights Item)
    {
      _context.Flight.Add(Item);
      _context.SaveChanges();
    }

    public Flights Find(long key)
    {
      return _context.Flight.FirstOrDefault(t => t.Key == key);
    }


    public void Remove(long key)
    {
      var entity = _context.Flight.First(t => t.Key == key);
      _context.Flight.Remove(entity);
      _context.SaveChanges();
    }

    public void Update(Flights item)
    {
      _context.Flight.Update(item);
      _context.SaveChanges();
    }
  }
}
