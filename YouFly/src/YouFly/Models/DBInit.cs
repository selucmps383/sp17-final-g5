﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouFly.Models
{
    public class DBInit
    {
        public static void Init(FlightContext context)
        {
            context.Database.EnsureCreated();

            if (context.Flight.Any())
            {
                return;
            }

            var cust = new Cust[]
            {
                new Cust { UserId = 1, Email = "admin@selu.edu", Fname = "Admin", Lname = "Istrator", Password = "password"  }
            };
            foreach(Cust c in cust)
            {
                context.Customer.Add(c);
            }
            context.SaveChanges();
        }
    }
}
