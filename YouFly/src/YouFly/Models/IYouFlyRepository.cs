﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouFly.Models;
using YouFly.Repository;

namespace YouFly.Repository
{
    public interface IYouFlyRepository
    {
        void Add(Flights item);
        IEnumerable<Flights> GetAll();
        Flights Find(long key);
        void Remove(long key);
        void Update(Flights item);
    }

}
