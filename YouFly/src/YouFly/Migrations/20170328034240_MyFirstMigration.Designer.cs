﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using YouFly.Models;

namespace YouFly.Migrations
{
    [DbContext(typeof(FlightContext))]
    [Migration("20170328034240_MyFirstMigration")]
    partial class MyFirstMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("YouFly.Models.Flights", b =>
                {
                    b.Property<long>("Key")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Day");

                    b.Property<int>("Length");

                    b.Property<string>("Name");

                    b.Property<DateTime>("Time");

                    b.Property<string>("arriveIATA");

                    b.Property<string>("departIATA");

                    b.Property<double>("hrsTravelled");

                    b.Property<double>("milesTravelled");

                    b.Property<int>("seatAmnt");

                    b.Property<double>("seatPrice");

                    b.Property<string>("seatType");

                    b.HasKey("Key");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("Flight");
                });
        }
    }
}
